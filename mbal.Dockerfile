FROM nginx:alpine

LABEL maintainer="Hino <sinhngay3110@gmail.com>"

RUN rm -r /usr/share/nginx/html
COPY html-mbal /usr/share/nginx/html

RUN rm /etc/nginx/conf.d/*
COPY conf.d/default.conf /etc/nginx/conf.d/

