### You may need
- `docker engine` 19.03+, `docker-compose` 1.28+
- generate favicon: https://favicon.io/favicon-converter/
- as always:
  ```bash
  docker-compose up -d && docker-compose logs -f
  ```
